#pragma once

#include "util.hh"

class Engine {
public:
  Engine();
  cl::Device get_device();
  cl::Program mk_prog(std::string);
  
private:
  cl::Device device;
  void init_device();
};