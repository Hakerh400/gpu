#include "util.hh"

int floor(double x){
  #undef floor
  return static_cast<int>(std::floor(x));
  #define floor _floor_aux
}

int ceil(double x){
  #undef ceil
  return static_cast<int>(std::ceil(x));
  #define ceil _ceil_aux
}

double sqrt(double x){
  #undef sqrt
  return std::sqrt(x);
  #define sqrt _sqrt_aux
}

int bound(int val_min, int val_max, int val){
  if(val < val_min) return val_min;
  if(val > val_max) return val_max;
  return val;
}

double bound(double val_min, double val_max, double val){
  if(val < val_min) return val_min;
  if(val > val_max) return val_max;
  return val;
}

double min(double a, double b){
  if(a < b) return a;
  return b;
}

double max(double a, double b){
  if(a > b) return a;
  return b;
}

double hypot(double x, double y){
  return sqrt(x * x + y * y);
}

double dist(double x1, double y1, double x2, double y2){
  return hypot(x2 - x1, y2 - y1);
}

double pow(double b, double e){
  #undef pow
  return std::pow(b, e);
  #define pow _pow_aux
}

double sin(double x){
  #undef sin
  return std::sin(x);
  #define sin _sin_aux
}

double cos(double x){
  #undef cos
  return std::cos(x);
  #define cos _cos_aux
}

double log_2(double x){
  return std::log2(x);
}

std::string read_file(const std::string& pth){
  std::ifstream st (pth);
  std::stringstream buf;
  buf << st.rdbuf();
  st.close();
  
  return buf.str();
}