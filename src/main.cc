#include "log.hh"
#include "engine.hh"
#include "main.hh"

void main1(){
  Engine* eng = new Engine();
  
  std::string cl_dir = "./src/cl";
  std::string kernel_file = cl_dir + "/test.cl";
  
  int err = 0;
  
  cl::Device device = eng->get_device();
  cl::Program prog = eng->mk_prog(kernel_file);
  cl::Context ctx = prog.getInfo<CL_PROGRAM_CONTEXT>();
  
  int buf_size = 100;
  size_t bytes_num = buf_size * sizeof(int);
  int* buf1 = new int[buf_size];
  int* buf2 = new int[buf_size];
  
  for(int i = 0; i != buf_size; i++)
    buf1[i] = i;
  
  int flags1 = 0;//CL_MEM_READ_ONLY;
  int flags2 = 0;//CL_MEM_WRITE_ONLY;
  cl::Buffer cl_buf1(ctx, flags1, bytes_num);
  cl::Buffer cl_buf2(ctx, flags2, bytes_num);
  cl::Kernel kernel(prog, "test", &err);
  
  kernel.setArg(0, cl_buf1);
  kernel.setArg(1, cl_buf2);
  
  int global_size = buf_size;
  int local_size = 1;
  
  cl::CommandQueue queue(ctx, device);
  err = queue.enqueueWriteBuffer(cl_buf1, false, 0, bytes_num, buf1);
  assert(!err);
  err = queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(global_size), cl::NDRange(local_size));
  assert(!err);
  err = queue.enqueueReadBuffer(cl_buf2, true, 0, bytes_num, buf2);
  assert(!err);
  
  log(std::vector<int>(buf2, buf2 + buf_size));
  
  delete[] buf1;
  delete[] buf2;
  delete eng;
}

int main(){
  main1();
  return 0;
}