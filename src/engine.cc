#include "log.hh"
#include "engine.hh"

Engine::Engine(){
  init_device();
}

void Engine::init_device(){
  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);
  
  int platforms_num = platforms.size();
  assert(platforms_num != 0);
  
  const cl::Platform& platform = platforms[0];
  std::vector<cl::Device> devices;
  platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
  
  int devices_num = devices.size();
  assert(devices_num != 0);
  
  device = devices[0];
}

cl::Device Engine::get_device(){
  return device;
}

cl::Program Engine::mk_prog(std::string pth){
  std::string kernel_src = read_file(pth);
  cl::Program::Sources sources(1, kernel_src);
  cl::Context ctx(device);
  cl::Program prog(ctx, sources);
  
  int err = prog.build();
  
  if(err){
    std::string msg = prog.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device);
    log(msg);
    exit(1);
  }
  
  return prog;
}