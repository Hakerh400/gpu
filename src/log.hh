#pragma once

#include "util.hh"

template<typename T>
void log_aux(T, bool endl=1);

void log();
void log(const char*, bool endl=1);
void log(int, bool endl=1);
void log(uint32_t, bool endl=1);
void log(uint64_t, bool endl=1);
void log(double, bool endl=1);
void log(const void*, bool endl=1);
void log(const std::string&, bool endl=1);

void logb();

template<typename T>
void log(const std::vector<T>& xs, bool endl=1){
  log("[", 0);

  int i = 0;

  for(const T& x : xs){
    if(i++ != 0) log(", ", 0);
    log(x, 0);
  }

  log("]", endl);
}

template<typename T>
void log(const std::set<T>& set, bool endl=1){
  log("{", 0);

  int i = 0;

  for(const T& x : set){
    if(i++ != 0) log(", ", 0);
    log(x, 0);
  }

  log("}", endl);
}

template<typename K, typename V>
void log(const std::map<K, V>& mp, bool endl=1){
  log("{", 0);

  int i = 0;

  for(const std::pair<K, V>& p : mp){
    if(i++ != 0) log(", ", 0);
    log("(", 0);
    log(p.first, 0);
    log(" -> ", 0);
    log(p.second, 0);
    log(")", 0);
  }

  log("}", endl);
}