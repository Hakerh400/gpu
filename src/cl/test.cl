__kernel void test(__global int* inp, __global int* volatile out){
  int n = 99;
  int i = atomic_inc(&(out[0]));
  i = ((i % n) + n) % n;
  out[1 + i] = get_global_id(0);
}